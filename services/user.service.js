const { saveData, getData } = require("../repositories/user.repository");

const getName = (user) => {
    if (user) {
        return user.name;
    } else {
        return null;
    }
};

const saveName = (user) => {
    if (user) {
        return saveData(user.name);
    } else {
        return null;
    }
};

const getUserById = (id) =>{
    const users = getData()
    const user = users.find(user => user._id === id)
    if(user){
        return user
    }else{
        return null
    }
}
const isUsersFieldsValid = (user) =>{
    const fields = ['_id', 'attack', 'defense', 'health', 'name', 'source'];
    const containAllFields = Object.keys(user).sort().toString() === fields.sort().toString()
    let result = {valid: true, message: ""};
    if(containAllFields){
        if(typeof (user.health) !== "number"){
            result.valid = false;
            result.message = "users health should be a number "
        }
        if(typeof (user.attack) !== "number"){
            result.valid = false;
            result.message = "users attack should be a number "
        }
        if(typeof (user.defense) !== "number"){
            result.valid = false;
            result.message = "users defence should be a number "
        }

    }else{
        result.valid = false;
        result.message = `user should contain all fields: ${fields.toString()}`
    }
    return result
}

const isUserExist = (id) => {
     return !!(getUserById(id))
}

const isUserValid = (user) => {
    let result = {valid: true, message: ""};
    if(!isUserExist(user._id)){
        result = isUsersFieldsValid(user);
    }else{
        result.valid = false;
        result.message = `user with id: ${user._id} already exist`;
    }
    console.log(result)
    return result
}

module.exports = {
    getName,
    saveName,
    getUserById,
    isUserValid,
    isUsersFieldsValid,
    isUserExist
};