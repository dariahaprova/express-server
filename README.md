# express-server

Available lifecycle scripts:

```sh
$ npm start
```

Acceptable requests: 
```sh
GET: /user
get an array of all users

GET: /user/:id
get one user by ID

POST: /user
create user according to the data from the request body

PUT: /user/:id
update user according to the data from the request body

DELETE: /user/:id
delete one user by ID
```