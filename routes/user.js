const express = require('express');
const router = express.Router();

const { getUserById, isUserValid, isUsersFieldsValid, isUserExist } = require("../services/user.service");
const { isAuthorized } = require("../middlewares/auth.middleware");
const {getData, addUser, changeUser, deleteUser} = require("../repositories/user.repository")

router.get('/', function(req, res, next) {
    const data = getData();
    if(data){
        res.json(data);
    }else {
        res.status(500).send(`error loading data`);
    }
});

router.get('/:id', function(req, res, next) {
    const data = getUserById(req.params.id);
    if(data){
        res.json(data);
    }else {
        res.status(404).send(`User not found`);
    }
});

router.post('/', function(req, res, next) {
    const valid = isUserValid(req.body)

    const result = valid.valid ? addUser(req.body) : false

    if (result) {
        res.send(`user added`);
    } else {
        res.status(400).send(valid.message);
    }
});

router.put('/:id', function(req, res, next) {
    if(req.params.id.localeCompare(req.body._id) !== 0){
        res.status(400).send(`Id in request param and inside body must be equal!`);
        return;
    }
    const exist = isUserExist(req.params.id)
    console.log(exist);
    if (!exist) {
        res.status(400).send(`can not find user with ${req.params.id} id`);
        return;
    }
    const validationResult = isUsersFieldsValid(req.body)
    const result = validationResult.valid ? changeUser(req.params.id, req.body) : false;
    if (result) {
        res.send(`user changed`);
    } else {
        res.status(400).send(validationResult.message);
    }

});

router.delete('/:id', function(req, res, next) {
    const exist = isUserExist(req.params.id)
    console.log(exist)
    const result = exist ? deleteUser(req.params.id) : false
    console.log(result)

    if (result) {
        res.send(`user deleted`);
    } else {
        res.status(400).send(`User with id: ${req.params.id} does not exist`);
    }
});



module.exports = router;